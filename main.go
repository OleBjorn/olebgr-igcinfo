package main

import (
	"encoding/json"
	"math/rand"
	"fmt"
	"github.com/marni/goigc"
	"net/http"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
)


//Structs--------------------------------------
type apiData struct {
	uptime string `json:"uptime"`
	info string `json:"info"`
	version string `json:"version"`
}

type Track struct {
	H_Date       time.Time `json:"Date"`
	Pilot        string    `json:"Pilot"`
	Glider       string    `json:"Glider Type"`
	Glider_id    string    `json:"Glider ID"`
	Track_length float64   `json:"TrackLength"`

}

type trackDB struct {
	tracks map[int]Track
}
//---------------------------------------------

//Global variables-----------------------------
var startTime time.Time
var api = apiData{
	uptime: timeFormat(startTime),
	info:"Service for IGC tracks.",
	version:"v1",
}
var db = trackDB{}
var IDs []int
//---------------------------------------------

//Track  funcs---------------------------------
func (db *trackDB) Init() {
	db.tracks = make(map[int]Track)
}

func (db *trackDB) Get(id int) (Track, bool) {
	t, ok := db.tracks[id]
	return t, ok
}

func (db *trackDB) GetField(field string, id int) (string, bool){
	isField := true
	val := reflect.ValueOf(db.tracks[id])
	fi := reflect.Indirect(val).FieldByName(field)
	if fi.String() == "<invalid Value>"{
		isField = false
	}
	return string(fi.String()),isField
}

func (db *trackDB) Add(t Track) int {
	//New ID
	id := rand.Int()
	_, ok := db.Get(id)

	//If exists, create new ID
	for ok {
		id = rand.Int()
		_, ok = db.Get(id)
	}

	db.tracks[id] = t

	return id

}
//---------------------------------------------

//ISO8601 format
func timeFormat (startTime time.Time) string{
	endTime := time.Now()
	return fmt.Sprintf("P%dY%dM%dDT%dH%dM%dS",
		endTime.Year()-startTime.Year(), endTime.Month()-startTime.Month(),
		endTime.Day()-startTime.Day(), endTime.Hour()-startTime.Hour(), endTime.Minute()-startTime.Minute(),
		endTime.Second()-startTime.Second())
}

//GET funcs------------------------------------
func GetAPIinfo(w *http.ResponseWriter){
	api.uptime = timeFormat(startTime)
	json.NewEncoder(*w).Encode(api)
}

func GetTrackID (w *http.ResponseWriter){
	if len(db.tracks) == 0{
		json.NewEncoder(*w).Encode([]Track{})
	} else {
		json.NewEncoder(*w).Encode(IDs)
	}
}

func GetTrack (w *http.ResponseWriter, db *trackDB, ID string){
	IntID, _ := strconv.Atoi(ID)
	tra, status := db.Get(IntID)
	if !status {
		http.Error(*w, http.StatusText(404), http.StatusNotFound)
		return
	}else{
		json.NewEncoder(*w).Encode(tra)
	}

}

func GetTrackField (w *http.ResponseWriter, db *trackDB, ID string, field string){
	IntID, _ := strconv.Atoi(ID)
	tra, status := db.Get(IntID)
	if !status {
		http.Error(*w, http.StatusText(404), http.StatusNotFound)
	}
	if field == "Track_length"{
		fmt.Fprint(*w, tra.Track_length)
	} else if  field == "H_Date" {
		fmt.Fprint(*w, tra.H_Date)
	} else {
		returnField, ok := db.GetField(field, IntID)
		if ok {
			fmt.Printf(returnField)
		} else {
			http.Error(*w, http.StatusText(404), http.StatusNotFound)
			return
		}
	}


}

//-----------------------------------------------

//Handler func-----------------------------------
func apiHandler(w http.ResponseWriter, r *http.Request){
	switch r.Method {
	case "POST":
		URL := strings.Split(r.URL.Path, "/")
		if len(URL) != 4{
			http.Error(w, http.StatusText(400), http.StatusBadRequest)
		}
		if URL[3] == "igc" {
			if r.Body == nil {
				http.Error(w, "Wrong URL", http.StatusBadRequest)
				return
			}
			//Local variables
			url := make(map[string]string)
			var tra Track

			err := json.NewDecoder(r.Body).Decode(&url)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			track, err := igc.ParseLocation(url["url"])
			if err != nil {
				http.Error(w, "Cant read track", http.StatusBadRequest)
				return
			}

			tra.Pilot = track.Pilot
			tra.H_Date = track.Header.Date
			tra.Track_length = track.Points[0].Distance(track.Points[len(track.Points)-1])
			tra.Glider = track.GliderType
			tra.Glider_id = track.GliderID

			id := db.Add(tra)
			IDs = append(IDs, id)
			json.NewEncoder(w).Encode(id)
			return

		} else {
			http.Error(w, http.StatusText(404), http.StatusBadRequest)
			return
		}

	case "GET":
		URL := strings.Split(r.URL.Path, "/")

		if len(URL) == 6 {
			http.Header.Add(w.Header(), "content-type", "text/plain")
		} else {
			http.Header.Add(w.Header(), "content-type", "application/json")
		}

		if URL[1] == "igcinfo" && URL[2] != "api" {
			http.Error(w, http.StatusText(404), http.StatusNotFound)
			return
		} else if len(URL) == 5 && URL[3] != "igc" || len(URL) == 6 && URL[3] != "igc" {
			http.Error(w, http.StatusText(404), http.StatusNotFound)
			return
		}

		if URL[2] == "api" && len(URL) == 3 {
			GetAPIinfo(&w) 		//Handel /api
		} else if URL[3] == "igc" && len(URL) == 4 {
			GetTrackID(&w)			//Handel /api/igc
		} else if URL[4] != "" && len(URL) == 5 {
			GetTrack(&w, &db, URL[4]) //Handel /api/igc/<id>
		} else if URL[5] != "" && len(URL) == 6 {
			GetTrackField(&w, &db, URL[4], URL[5])
		} else {
			http.Error(w, http.StatusText(404), http.StatusNotFound)
			return
		}
	}

}
//-----------------------------------------------

//Main
func main() {
	db.Init()
	startTime = time.Now()

	var p string
	if port := os.Getenv("PORT"); port != "" {
		p = ":" + port
	} else {
		p = ":8080"
	}
	http.HandleFunc("/igcinfo/", apiHandler)
	http.ListenAndServe(p, nil)

}

